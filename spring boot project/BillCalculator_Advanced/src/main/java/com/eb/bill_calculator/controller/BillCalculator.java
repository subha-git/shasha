package com.eb.bill_calculator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class BillCalculator {

	@RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/calculateBill")
    public String calculateBill(@RequestParam double units, Model model) {
        double ratePerUnit;
        if (units <= 100) {
            ratePerUnit = 1.5;
        } else if (units <= 300) {
            ratePerUnit = 2.5;
        } else {
            ratePerUnit = 4.0;
        }
        double totalAmount = units * ratePerUnit;
        model.addAttribute("totalAmount", totalAmount);
        return "result";
    }
}
