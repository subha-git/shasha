package com.eb.bill_calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillCalculatorApplication.class, args);
	}

}
