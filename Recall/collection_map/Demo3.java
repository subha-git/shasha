package collection_map;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Demo3 {

	public static void main(String[] args) {
		  HashMap hm = new HashMap(); 
		    hm.put("Chicken",110);
		    hm.put("Mutton", 130);
		    hm.put("Veg Biryani", 100);
		    hm.put("Veg Fried Rice", 100);
		    System.out.println(hm);
		    //System.out.println(hm.get("Mutton"));
		    Set menu =  hm.entrySet();
		  for(Object obj:menu)
		  {
		    Entry entry = (Entry)obj;
		    Object menu_item = entry.getKey();
		    if(menu_item.equals("Chicken"))
		    {
		      System.out.println(entry.getValue());
		    }
		  }
	}

}
