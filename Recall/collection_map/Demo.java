package collection_map;

import java.util.HashMap;
import java.util.Set;

public class Demo {

	public static void main(String[] args) {
		HashMap hm = new HashMap(); 
	    hm.put("Dhoni", 55);
	    hm.put("Dube", 50);
	    hm.put("Jadeja", 60);
	    hm.put("Kohli", 67);
	    System.out.println(hm);
	    System.out.println(hm.get("Dhoni"));
	    Set entrySet =  hm.entrySet();
	    for(Object ob: entrySet)
	      System.out.println(ob);
				
	}

}
