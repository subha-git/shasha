package collection_map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Demo2 {

	public static void main(String[] args) {
		 HashMap hm = new HashMap(); 
		    hm.put("Chicken",110);
		    hm.put("Mutton", 130);
		    hm.put("Veg Biryani", 100);
		    hm.put("Veg Fried Rice", 100);
		    System.out.println(hm);
		    System.out.println(hm.get("Mutton"));
		    Set playerScore =  hm.entrySet();
		    System.out.println(playerScore);
		    
		    System.out.println(hm.keySet());
		    System.out.println(hm.values());
		    
		    Collection priceCollection = hm.values();
		    int total = 0; 
		    for(Object price:priceCollection)
		    {
		      total = total + (int)price; 
		    }
		  System.out.println(total);
	}

}
