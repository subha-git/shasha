package practice;

public class Pattern2 {

    public static void main(String[] args) {
        Pattern2 pattern = new Pattern2();
        int n = 5;
        pattern.printPyramid(n);
        Pattern2 ptn = new Pattern2();
        ptn.anotherprymid(n);
        
    }

    private void anotherprymid(int n) {
    	for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print("  "); // Print spaces
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(i+ " ");
            }
            System.out.println(); // Move to the next line after each row
        }
		
	}

	private void printPyramid(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print("  "); // Print spaces
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k+ " ");
            }
            System.out.println(); // Move to the next line after each row
        }
    }
}
