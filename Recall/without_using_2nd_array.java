package practice;
import java.util.Scanner;

public class without_using_2nd_array {
	 public static void main(String[] args) {
	        Scanner sc=new Scanner(System.in);
	        System.out.println("Enter the array size");
	        int n=sc.nextInt();
	        System.out.println("Enter the values for array");
	        int[] arr=new int[n];
	        for(int i=0;i<n;i++) {
	            arr[i]=sc.nextInt();
	        }

	        // Find duplicates
	        for(int i=0;i<n;i++) {
	            if (arr[i] != -1) {
	                int count = 1;
	                for(int j=i+1;j<n;j++) {
	                    if(arr[i]==arr[j]) {
	                        count++;
	                        arr[j] = -1; // Mark duplicate as -1
	                    }
	                }
	                if(count>1) {
	                    System.out.println(arr[i] + " is a duplicate");
	                    System.out.println(arr[i] + " occurs " + count + " times");
	                }
	                else {
	                    System.out.println(arr[i] + " is unique ");
	                }
	            }
	        }
	    }
	}


