package constructor;

public class Demo {
//	default constructor or zero argument constructor
	Demo(){
		System.out.println("constructor learning");
	}
 //	parameterized constructor or argument constructor
	int age;
	String name;
	Demo(int age,String name){
		this.age=age;
		this.name=name;
		System.out.println("Name :"+name+"'s" +" Age is:"+age);
		
	}

	public static void main(String[] args) {
		Demo myobj=new Demo();
		Demo obj=new Demo( 25,"subha");
		
	}

}
