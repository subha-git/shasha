package practice;

import java.util.Scanner;

public class Array_demo {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size");
		int n=sc.nextInt();
		System.out.println("Enter the values for array");
		int[]arr=new int[n];
		int[]freq=new int[n];
		for(int i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
//		find the duplicate number
		for(int i=0;i<arr.length;i++) 
		{
		int	count=1;
			for(int j=i+1;j<arr.length;j++) 
			{
				if(arr[i]==arr[j]) 
				{
					count++;
					freq[j]=-1;
				}
				
			}
			if(freq[i]!=-1)
			{
				freq[i]=count;
			}
			
		}
		
		for(int i=0;i<arr.length;i++) 
		{
			if(freq[i]>1) 
			{
				System.out.println("" +arr[i]+" is duplicates");
				System.out.println(arr[i]+" occurs "+freq[i]+" times");
				
			}
			else if (freq[i]==1){
				System.out.println(arr[i]+"is unique ");
			}
			
		}
	}
		
	}
